'use strict';

keretApp.factory('Consumption', function ($resource) {
        return $resource('app/rest/consumptions/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });
