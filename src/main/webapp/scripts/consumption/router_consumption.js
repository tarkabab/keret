'use strict';

keretApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/consumption', {
                    templateUrl: 'views/consumptions.html',
                    controller: 'ConsumptionController',
                    resolve:{
                        resolvedConsumption: ['Consumption', function (Consumption) {
                            return Consumption.query().$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
