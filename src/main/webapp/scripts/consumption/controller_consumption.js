'use strict';

keretApp.controller('ConsumptionController', function ($scope, resolvedConsumption, Consumption) {

        $scope.consumptions = resolvedConsumption;

        $scope.create = function () {
            Consumption.save($scope.consumption,
                function () {
                    $scope.consumptions = Consumption.query();
                    $('#saveConsumptionModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.consumption = Consumption.get({id: id});
            $('#saveConsumptionModal').modal('show');
        };

        $scope.delete = function (id) {
            Consumption.delete({id: id},
                function () {
                    $scope.consumptions = Consumption.query();
                });
        };

        $scope.clear = function () {
            $scope.consumption = {goodie: null, quantity: null, datetime: null, id: null};
        };
    });
