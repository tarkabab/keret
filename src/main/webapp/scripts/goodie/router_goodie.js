'use strict';

keretApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/goodie', {
                    templateUrl: 'views/goodies.html',
                    controller: 'GoodieController',
                    resolve:{
                        resolvedGoodie: ['Goodie', function (Goodie) {
                            return Goodie.query();
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
