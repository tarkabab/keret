'use strict';

keretApp.factory('Goodie', function ($resource) {
        return $resource('app/rest/goodies/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });
