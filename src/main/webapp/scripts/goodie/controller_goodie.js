'use strict';

keretApp.controller('GoodieController', function ($scope, resolvedGoodie, Goodie) {

        $scope.goodies = resolvedGoodie;

        $scope.create = function () {
            Goodie.save($scope.goodie,
                function () {
                    $scope.goodies = Goodie.query();
                    $('#saveGoodieModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.goodie = Goodie.get({id: id});
            $('#saveGoodieModal').modal('show');
        };

        $scope.delete = function (id) {
            Goodie.delete({id: id},
                function () {
                    $scope.goodies = Goodie.query();
                });
        };

        $scope.clear = function () {
            $scope.goodie = {category: null, name: null, price: null, quantity: null, id: null};
        };
    });
