'use strict';

keretApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/member', {
                    templateUrl: 'views/members.html',
                    controller: 'MemberController',
                    resolve:{
                        resolvedMember: ['Member', function (Member) {
                            return Member.query().$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
