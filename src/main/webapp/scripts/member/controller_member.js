'use strict';

keretApp.controller('MemberController', function ($scope, resolvedMember, Member) {

        $scope.members = resolvedMember;

        $scope.create = function () {
            Member.save($scope.member,
                function () {
                    $scope.members = Member.query();
                    $('#saveMemberModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.member = Member.get({id: id});
            $('#saveMemberModal').modal('show');
        };

        $scope.delete = function (id) {
            Member.delete({id: id},
                function () {
                    $scope.members = Member.query();
                });
        };

        $scope.clear = function () {
            $scope.member = {firstname: null, lastname: null, placeofbirth: null, timeofbirth: null, mothersname: null, taxid: null, email: null, phonenumber: null, memberid: null, nfckey: null, balance: null, id: null};
        };
    });
