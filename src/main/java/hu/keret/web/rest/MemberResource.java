package hu.keret.web.rest;

import com.codahale.metrics.annotation.Timed;
import hu.keret.domain.Member;
import hu.keret.repository.MemberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Member.
 */
@RestController
@RequestMapping("/app")
public class MemberResource {

    private final Logger log = LoggerFactory.getLogger(MemberResource.class);

    @Inject
    private MemberRepository memberRepository;

    /**
     * POST  /rest/members -> Create a new member.
     */
    @RequestMapping(value = "/rest/members",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Member member) {
        log.debug("REST request to save Member : {}", member);
        memberRepository.save(member);
    }

    /**
     * GET  /rest/members -> get all the members.
     */
    @RequestMapping(value = "/rest/members",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Member> getAll() {
        log.debug("REST request to get all Members");
        return memberRepository.findAll();
    }

    /**
     * GET  /rest/members/:id -> get the "id" member.
     */
    @RequestMapping(value = "/rest/members/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Member> get(@PathVariable String id, HttpServletResponse response) {
        log.debug("REST request to get Member : {}", id);
        Member member = memberRepository.findOne(id);
        if (member == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(member, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/members/:id -> delete the "id" member.
     */
    @RequestMapping(value = "/rest/members/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable String id) {
        log.debug("REST request to delete Member : {}", id);
        memberRepository.delete(id);
    }
}
