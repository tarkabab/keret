package hu.keret.web.rest;

import com.codahale.metrics.annotation.Timed;
import hu.keret.domain.Goodie;
import hu.keret.repository.GoodieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Goodie.
 */
@RestController
@RequestMapping("/app")
public class GoodieResource {

    private final Logger log = LoggerFactory.getLogger(GoodieResource.class);

    @Inject
    private GoodieRepository goodieRepository;

    /**
     * POST  /rest/goodies -> Create a new goodie.
     */
    @RequestMapping(value = "/rest/goodies",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Goodie goodie) {
        log.debug("REST request to save Goodie : {}", goodie);
        goodieRepository.save(goodie);
    }

    /**
     * GET  /rest/goodies -> get all the goodies.
     */
    @RequestMapping(value = "/rest/goodies",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Goodie> getAll() {
        log.debug("REST request to get all Goodies");
        return goodieRepository.findAll();
    }

    /**
     * GET  /rest/goodies/:id -> get the "id" goodie.
     */
    @RequestMapping(value = "/rest/goodies/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Goodie> get(@PathVariable String id, HttpServletResponse response) {
        log.debug("REST request to get Goodie : {}", id);
        Goodie goodie = goodieRepository.findOne(id);
        if (goodie == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(goodie, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/goodies/:id -> delete the "id" goodie.
     */
    @RequestMapping(value = "/rest/goodies/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable String id) {
        log.debug("REST request to delete Goodie : {}", id);
        goodieRepository.delete(id);
    }
}
