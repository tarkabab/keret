package hu.keret.web.rest;

import com.codahale.metrics.annotation.Timed;
import hu.keret.domain.Consumption;
import hu.keret.repository.ConsumptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Consumption.
 */
@RestController
@RequestMapping("/app")
public class ConsumptionResource {

    private final Logger log = LoggerFactory.getLogger(ConsumptionResource.class);

    @Inject
    private ConsumptionRepository consumptionRepository;

    /**
     * POST  /rest/consumptions -> Create a new consumption.
     */
    @RequestMapping(value = "/rest/consumptions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Consumption consumption) {
        log.debug("REST request to save Consumption : {}", consumption);
        consumptionRepository.save(consumption);
    }

    /**
     * GET  /rest/consumptions -> get all the consumptions.
     */
    @RequestMapping(value = "/rest/consumptions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Consumption> getAll() {
        log.debug("REST request to get all Consumptions");
        return consumptionRepository.findAll();
    }

    /**
     * GET  /rest/consumptions/:id -> get the "id" consumption.
     */
    @RequestMapping(value = "/rest/consumptions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Consumption> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Consumption : {}", id);
        Consumption consumption = consumptionRepository.findOne(id);
        if (consumption == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(consumption, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/consumptions/:id -> delete the "id" consumption.
     */
    @RequestMapping(value = "/rest/consumptions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Consumption : {}", id);
        consumptionRepository.delete(id);
    }
}
