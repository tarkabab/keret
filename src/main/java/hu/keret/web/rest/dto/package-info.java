/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package hu.keret.web.rest.dto;
