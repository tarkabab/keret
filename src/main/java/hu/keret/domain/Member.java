package hu.keret.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import hu.keret.domain.util.CustomDateTimeDeserializer;
import hu.keret.domain.util.CustomDateTimeSerializer;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A Member.
 */
@Entity
@Table(name = "T_MEMBER")
public class Member implements Serializable {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "placeofbirth")
    private String placeofbirth;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "timeofbirth")
    private DateTime timeofbirth;

    @Column(name = "mothersname")
    private String mothersname;

    @Column(name = "taxid")
    private String taxid;

    @Column(name = "email")
    private String email;

    @Column(name = "phonenumber")
    private String phonenumber;

    @Column(name = "memberid")
    private String memberid;

    @Column(name = "nfckey")
    private String nfckey;

    @Column(name = "balance")
    private BigDecimal balance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPlaceofbirth() {
        return placeofbirth;
    }

    public void setPlaceofbirth(String placeofbirth) {
        this.placeofbirth = placeofbirth;
    }

    public DateTime getTimeofbirth() {
        return timeofbirth;
    }

    public void setTimeofbirth(DateTime timeofbirth) {
        this.timeofbirth = timeofbirth;
    }

    public String getMothersname() {
        return mothersname;
    }

    public void setMothersname(String mothersname) {
        this.mothersname = mothersname;
    }

    public String getTaxid() {
        return taxid;
    }

    public void setTaxid(String taxid) {
        this.taxid = taxid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getNfckey() {
        return nfckey;
    }

    public void setNfckey(String nfckey) {
        this.nfckey = nfckey;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Member member = (Member) o;

        if (id != null ? !id.equals(member.id) : member.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", firstname='" + firstname + "'" +
                ", lastname='" + lastname + "'" +
                ", placeofbirth='" + placeofbirth + "'" +
                ", timeofbirth='" + timeofbirth + "'" +
                ", mothersname='" + mothersname + "'" +
                ", taxid='" + taxid + "'" +
                ", email='" + email + "'" +
                ", phonenumber='" + phonenumber + "'" +
                ", memberid='" + memberid + "'" +
                ", nfckey='" + nfckey + "'" +
                ", balance='" + balance + "'" +
                '}';
    }
}
