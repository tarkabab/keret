package hu.keret.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import hu.keret.domain.util.CustomDateTimeDeserializer;
import hu.keret.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A Consumption.
 */
@Entity
@Table(name = "T_CONSUMPTION")
public class Consumption implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "goodie")
    private String goodie;

    @Column(name = "quantity", precision=10, scale=2)
    private BigDecimal quantity;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "datetime", nullable = false)
    private DateTime datetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGoodie() {
        return goodie;
    }

    public void setGoodie(String goodie) {
        this.goodie = goodie;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public DateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(DateTime datetime) {
        this.datetime = datetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Consumption consumption = (Consumption) o;

        if (id != null ? !id.equals(consumption.id) : consumption.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Consumption{" +
                "id=" + id +
                ", goodie='" + goodie + "'" +
                ", quantity='" + quantity + "'" +
                ", datetime='" + datetime + "'" +
                '}';
    }
}
