package hu.keret.repository;

import hu.keret.domain.Goodie;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repository for the Goodie entity.
 */
public interface GoodieRepository extends JpaRepository<Goodie, String> {

}
