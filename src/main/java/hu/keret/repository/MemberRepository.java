package hu.keret.repository;

import hu.keret.domain.Member;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data MongoDB repository for the Member entity.
 */
public interface MemberRepository extends JpaRepository<Member, String> {

}
