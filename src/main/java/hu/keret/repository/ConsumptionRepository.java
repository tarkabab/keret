package hu.keret.repository;

import hu.keret.domain.Consumption;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Consumption entity.
 */
public interface ConsumptionRepository extends JpaRepository<Consumption, Long> {

}
