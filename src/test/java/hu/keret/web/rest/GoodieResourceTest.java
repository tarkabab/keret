package hu.keret.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import hu.keret.Application;
import hu.keret.domain.Goodie;
import hu.keret.repository.GoodieRepository;


/**
 * Test class for the GoodieResource REST controller.
 *
 * @see GoodieResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class })
public class GoodieResourceTest {
    
    private static final String DEFAULT_ID = "1";
    
    private static final String DEFAULT_CATEGORY = "SAMPLE_TEXT";
    private static final String UPDATED_CATEGORY = "UPDATED_TEXT";
        
    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";
        
    private static final String DEFAULT_PRICE = "SAMPLE_TEXT";
    private static final String UPDATED_PRICE = "UPDATED_TEXT";
        
    private static final Integer DEFAULT_QUANTITY = 0;
    private static final Integer UPDATED_QUANTITY = 1;
        
    @Inject
    private GoodieRepository goodieRepository;

    private MockMvc restGoodieMockMvc;

    private Goodie goodie;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GoodieResource goodieResource = new GoodieResource();
        ReflectionTestUtils.setField(goodieResource, "goodieRepository", goodieRepository);

        this.restGoodieMockMvc = MockMvcBuilders.standaloneSetup(goodieResource).build();

        goodie = new Goodie();
        goodie.setId(DEFAULT_ID);

        goodie.setCategory(DEFAULT_CATEGORY);
        goodie.setName(DEFAULT_NAME);
        goodie.setPrice(DEFAULT_PRICE);
        goodie.setQuantity(DEFAULT_QUANTITY);
    }

    @Test
    public void testCRUDGoodie() throws Exception {

        // Create Goodie
        restGoodieMockMvc.perform(post("/app/rest/goodies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(goodie)))
                .andExpect(status().isOk());

        // Read Goodie
        restGoodieMockMvc.perform(get("/app/rest/goodies/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID))
                .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()))
                .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.toString()))
                .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY));

        // Update Goodie
        goodie.setCategory(UPDATED_CATEGORY);
        goodie.setName(UPDATED_NAME);
        goodie.setPrice(UPDATED_PRICE);
        goodie.setQuantity(UPDATED_QUANTITY);

        restGoodieMockMvc.perform(post("/app/rest/goodies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(goodie)))
                .andExpect(status().isOk());

        // Read updated Goodie
        restGoodieMockMvc.perform(get("/app/rest/goodies/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID))
                .andExpect(jsonPath("$.category").value(UPDATED_CATEGORY.toString()))
                .andExpect(jsonPath("$.name").value(UPDATED_NAME.toString()))
                .andExpect(jsonPath("$.price").value(UPDATED_PRICE.toString()))
                .andExpect(jsonPath("$.quantity").value(UPDATED_QUANTITY));

        // Delete Goodie
        restGoodieMockMvc.perform(delete("/app/rest/goodies/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Read nonexisting Goodie
        restGoodieMockMvc.perform(get("/app/rest/goodies/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());

    }
}
