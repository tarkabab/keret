package hu.keret.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.math.BigDecimal;
import java.util.List;

import hu.keret.Application;
import hu.keret.domain.Consumption;
import hu.keret.repository.ConsumptionRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConsumptionResource REST controller.
 *
 * @see ConsumptionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ConsumptionResourceTest {
   private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

    private static final String DEFAULT_GOODIE = "SAMPLE_TEXT";
    private static final String UPDATED_GOODIE = "UPDATED_TEXT";
    
    private static final BigDecimal DEFAULT_QUANTITY = BigDecimal.ZERO;
    private static final BigDecimal UPDATED_QUANTITY = BigDecimal.ONE;
    
   private static final DateTime DEFAULT_DATETIME = new DateTime(0L);
   private static final DateTime UPDATED_DATETIME = new DateTime().withMillisOfSecond(0);
   private static final String DEFAULT_DATETIME_STR = dateTimeFormatter.print(DEFAULT_DATETIME);
    

    @Inject
    private ConsumptionRepository consumptionRepository;

    private MockMvc restConsumptionMockMvc;

    private Consumption consumption;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConsumptionResource consumptionResource = new ConsumptionResource();
        ReflectionTestUtils.setField(consumptionResource, "consumptionRepository", consumptionRepository);
        this.restConsumptionMockMvc = MockMvcBuilders.standaloneSetup(consumptionResource).build();
    }

    @Before
    public void initTest() {
        consumption = new Consumption();
        consumption.setGoodie(DEFAULT_GOODIE);
        consumption.setQuantity(DEFAULT_QUANTITY);
        consumption.setDatetime(DEFAULT_DATETIME);
    }

    @Test
    @Transactional
    public void createConsumption() throws Exception {
        // Validate the database is empty
        assertThat(consumptionRepository.findAll()).hasSize(0);

        // Create the Consumption
        restConsumptionMockMvc.perform(post("/app/rest/consumptions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(consumption)))
                .andExpect(status().isOk());

        // Validate the Consumption in the database
        List<Consumption> consumptions = consumptionRepository.findAll();
        assertThat(consumptions).hasSize(1);
        Consumption testConsumption = consumptions.iterator().next();
        assertThat(testConsumption.getGoodie()).isEqualTo(DEFAULT_GOODIE);
        assertThat(testConsumption.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testConsumption.getDatetime()).isEqualTo(DEFAULT_DATETIME);
    }

    @Test
    @Transactional
    public void getAllConsumptions() throws Exception {
        // Initialize the database
        consumptionRepository.saveAndFlush(consumption);

        // Get all the consumptions
        restConsumptionMockMvc.perform(get("/app/rest/consumptions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(consumption.getId().intValue()))
                .andExpect(jsonPath("$.[0].goodie").value(DEFAULT_GOODIE.toString()))
                .andExpect(jsonPath("$.[0].quantity").value(DEFAULT_QUANTITY.intValue()))
                .andExpect(jsonPath("$.[0].datetime").value(DEFAULT_DATETIME_STR));
    }

    @Test
    @Transactional
    public void getConsumption() throws Exception {
        // Initialize the database
        consumptionRepository.saveAndFlush(consumption);

        // Get the consumption
        restConsumptionMockMvc.perform(get("/app/rest/consumptions/{id}", consumption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(consumption.getId().intValue()))
            .andExpect(jsonPath("$.goodie").value(DEFAULT_GOODIE.toString()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY.intValue()))
            .andExpect(jsonPath("$.datetime").value(DEFAULT_DATETIME_STR));
    }

    @Test
    @Transactional
    public void getNonExistingConsumption() throws Exception {
        // Get the consumption
        restConsumptionMockMvc.perform(get("/app/rest/consumptions/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConsumption() throws Exception {
        // Initialize the database
        consumptionRepository.saveAndFlush(consumption);

        // Update the consumption
        consumption.setGoodie(UPDATED_GOODIE);
        consumption.setQuantity(UPDATED_QUANTITY);
        consumption.setDatetime(UPDATED_DATETIME);
        restConsumptionMockMvc.perform(post("/app/rest/consumptions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(consumption)))
                .andExpect(status().isOk());

        // Validate the Consumption in the database
        List<Consumption> consumptions = consumptionRepository.findAll();
        assertThat(consumptions).hasSize(1);
        Consumption testConsumption = consumptions.iterator().next();
        assertThat(testConsumption.getGoodie()).isEqualTo(UPDATED_GOODIE);
        assertThat(testConsumption.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testConsumption.getDatetime()).isEqualTo(UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void deleteConsumption() throws Exception {
        // Initialize the database
        consumptionRepository.saveAndFlush(consumption);

        // Get the consumption
        restConsumptionMockMvc.perform(delete("/app/rest/consumptions/{id}", consumption.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Consumption> consumptions = consumptionRepository.findAll();
        assertThat(consumptions).hasSize(0);
    }
}
