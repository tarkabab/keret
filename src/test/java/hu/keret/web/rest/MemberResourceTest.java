package hu.keret.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.math.BigDecimal;
import java.util.List;

import hu.keret.Application;
import hu.keret.domain.Member;
import hu.keret.repository.MemberRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MemberResource REST controller.
 *
 * @see MemberResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class MemberResourceTest {
   private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

    private static final String DEFAULT_FIRSTNAME = "SAMPLE_TEXT";
    private static final String UPDATED_FIRSTNAME = "UPDATED_TEXT";
    
    private static final String DEFAULT_LASTNAME = "SAMPLE_TEXT";
    private static final String UPDATED_LASTNAME = "UPDATED_TEXT";
    
    private static final String DEFAULT_PLACEOFBIRTH = "SAMPLE_TEXT";
    private static final String UPDATED_PLACEOFBIRTH = "UPDATED_TEXT";
    
   private static final DateTime DEFAULT_TIMEOFBIRTH = new DateTime(0L);
   private static final DateTime UPDATED_TIMEOFBIRTH = new DateTime().withMillisOfSecond(0);
   private static final String DEFAULT_TIMEOFBIRTH_STR = dateTimeFormatter.print(DEFAULT_TIMEOFBIRTH);
    
    private static final String DEFAULT_MOTHERSNAME = "SAMPLE_TEXT";
    private static final String UPDATED_MOTHERSNAME = "UPDATED_TEXT";
    
    private static final String DEFAULT_TAXID = "SAMPLE_TEXT";
    private static final String UPDATED_TAXID = "UPDATED_TEXT";
    
    private static final String DEFAULT_EMAIL = "SAMPLE_TEXT";
    private static final String UPDATED_EMAIL = "UPDATED_TEXT";
    
    private static final String DEFAULT_PHONENUMBER = "SAMPLE_TEXT";
    private static final String UPDATED_PHONENUMBER = "UPDATED_TEXT";
    
    private static final String DEFAULT_MEMBERID = "SAMPLE_TEXT";
    private static final String UPDATED_MEMBERID = "UPDATED_TEXT";
    
    private static final String DEFAULT_NFCKEY = "SAMPLE_TEXT";
    private static final String UPDATED_NFCKEY = "UPDATED_TEXT";
    
    private static final BigDecimal DEFAULT_BALANCE = BigDecimal.ZERO;
    private static final BigDecimal UPDATED_BALANCE = BigDecimal.ONE;
    

    @Inject
    private MemberRepository memberRepository;

    private MockMvc restMemberMockMvc;

    private Member member;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MemberResource memberResource = new MemberResource();
        ReflectionTestUtils.setField(memberResource, "memberRepository", memberRepository);
        this.restMemberMockMvc = MockMvcBuilders.standaloneSetup(memberResource).build();
    }

    @Before
    public void initTest() {
        memberRepository.deleteAll();
        member = new Member();
        member.setFirstname(DEFAULT_FIRSTNAME);
        member.setLastname(DEFAULT_LASTNAME);
        member.setPlaceofbirth(DEFAULT_PLACEOFBIRTH);
        member.setTimeofbirth(DEFAULT_TIMEOFBIRTH);
        member.setMothersname(DEFAULT_MOTHERSNAME);
        member.setTaxid(DEFAULT_TAXID);
        member.setEmail(DEFAULT_EMAIL);
        member.setPhonenumber(DEFAULT_PHONENUMBER);
        member.setMemberid(DEFAULT_MEMBERID);
        member.setNfckey(DEFAULT_NFCKEY);
        member.setBalance(DEFAULT_BALANCE);
    }

    @Test
    public void createMember() throws Exception {
        // Validate the database is empty
        assertThat(memberRepository.findAll()).hasSize(0);

        // Create the Member
        restMemberMockMvc.perform(post("/app/rest/members")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(member)))
                .andExpect(status().isOk());

        // Validate the Member in the database
        List<Member> members = memberRepository.findAll();
        assertThat(members).hasSize(1);
        Member testMember = members.iterator().next();
        assertThat(testMember.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testMember.getLastname()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(testMember.getPlaceofbirth()).isEqualTo(DEFAULT_PLACEOFBIRTH);
        assertThat(testMember.getTimeofbirth()).isEqualTo(DEFAULT_TIMEOFBIRTH);
        assertThat(testMember.getMothersname()).isEqualTo(DEFAULT_MOTHERSNAME);
        assertThat(testMember.getTaxid()).isEqualTo(DEFAULT_TAXID);
        assertThat(testMember.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testMember.getPhonenumber()).isEqualTo(DEFAULT_PHONENUMBER);
        assertThat(testMember.getMemberid()).isEqualTo(DEFAULT_MEMBERID);
        assertThat(testMember.getNfckey()).isEqualTo(DEFAULT_NFCKEY);
        assertThat(testMember.getBalance()).isEqualTo(DEFAULT_BALANCE);
    }

    @Test
    public void getAllMembers() throws Exception {
        // Initialize the database
        memberRepository.save(member);

        // Get all the members
        restMemberMockMvc.perform(get("/app/rest/members"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(member.getId()))
                .andExpect(jsonPath("$.[0].firstname").value(DEFAULT_FIRSTNAME.toString()))
                .andExpect(jsonPath("$.[0].lastname").value(DEFAULT_LASTNAME.toString()))
                .andExpect(jsonPath("$.[0].placeofbirth").value(DEFAULT_PLACEOFBIRTH.toString()))
                .andExpect(jsonPath("$.[0].timeofbirth").value(DEFAULT_TIMEOFBIRTH_STR))
                .andExpect(jsonPath("$.[0].mothersname").value(DEFAULT_MOTHERSNAME.toString()))
                .andExpect(jsonPath("$.[0].taxid").value(DEFAULT_TAXID.toString()))
                .andExpect(jsonPath("$.[0].email").value(DEFAULT_EMAIL.toString()))
                .andExpect(jsonPath("$.[0].phonenumber").value(DEFAULT_PHONENUMBER.toString()))
                .andExpect(jsonPath("$.[0].memberid").value(DEFAULT_MEMBERID.toString()))
                .andExpect(jsonPath("$.[0].nfckey").value(DEFAULT_NFCKEY.toString()))
                .andExpect(jsonPath("$.[0].balance").value(DEFAULT_BALANCE.intValue()));
    }

    @Test
    public void getMember() throws Exception {
        // Initialize the database
        memberRepository.save(member);

        // Get the member
        restMemberMockMvc.perform(get("/app/rest/members/{id}", member.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(member.getId()))
            .andExpect(jsonPath("$.firstname").value(DEFAULT_FIRSTNAME.toString()))
            .andExpect(jsonPath("$.lastname").value(DEFAULT_LASTNAME.toString()))
            .andExpect(jsonPath("$.placeofbirth").value(DEFAULT_PLACEOFBIRTH.toString()))
            .andExpect(jsonPath("$.timeofbirth").value(DEFAULT_TIMEOFBIRTH_STR))
            .andExpect(jsonPath("$.mothersname").value(DEFAULT_MOTHERSNAME.toString()))
            .andExpect(jsonPath("$.taxid").value(DEFAULT_TAXID.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phonenumber").value(DEFAULT_PHONENUMBER.toString()))
            .andExpect(jsonPath("$.memberid").value(DEFAULT_MEMBERID.toString()))
            .andExpect(jsonPath("$.nfckey").value(DEFAULT_NFCKEY.toString()))
            .andExpect(jsonPath("$.balance").value(DEFAULT_BALANCE.intValue()));
    }

    @Test
    public void getNonExistingMember() throws Exception {
        // Get the member
        restMemberMockMvc.perform(get("/app/rest/members/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateMember() throws Exception {
        // Initialize the database
        memberRepository.save(member);

        // Update the member
        member.setFirstname(UPDATED_FIRSTNAME);
        member.setLastname(UPDATED_LASTNAME);
        member.setPlaceofbirth(UPDATED_PLACEOFBIRTH);
        member.setTimeofbirth(UPDATED_TIMEOFBIRTH);
        member.setMothersname(UPDATED_MOTHERSNAME);
        member.setTaxid(UPDATED_TAXID);
        member.setEmail(UPDATED_EMAIL);
        member.setPhonenumber(UPDATED_PHONENUMBER);
        member.setMemberid(UPDATED_MEMBERID);
        member.setNfckey(UPDATED_NFCKEY);
        member.setBalance(UPDATED_BALANCE);
        restMemberMockMvc.perform(post("/app/rest/members")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(member)))
                .andExpect(status().isOk());

        // Validate the Member in the database
        List<Member> members = memberRepository.findAll();
        assertThat(members).hasSize(1);
        Member testMember = members.iterator().next();
        assertThat(testMember.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testMember.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testMember.getPlaceofbirth()).isEqualTo(UPDATED_PLACEOFBIRTH);
        assertThat(testMember.getTimeofbirth()).isEqualTo(UPDATED_TIMEOFBIRTH);
        assertThat(testMember.getMothersname()).isEqualTo(UPDATED_MOTHERSNAME);
        assertThat(testMember.getTaxid()).isEqualTo(UPDATED_TAXID);
        assertThat(testMember.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testMember.getPhonenumber()).isEqualTo(UPDATED_PHONENUMBER);
        assertThat(testMember.getMemberid()).isEqualTo(UPDATED_MEMBERID);
        assertThat(testMember.getNfckey()).isEqualTo(UPDATED_NFCKEY);
        assertThat(testMember.getBalance()).isEqualTo(UPDATED_BALANCE);;
    }

    @Test
    public void deleteMember() throws Exception {
        // Initialize the database
        memberRepository.save(member);

        // Get the member
        restMemberMockMvc.perform(delete("/app/rest/members/{id}", member.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Member> members = memberRepository.findAll();
        assertThat(members).hasSize(0);
    }
}
