<?PHP
if (!defined("LOAD")) {
    if(!include("../sys/load.php"))
    {
    die("CORE ERROR!!");
    }
} else {
    define("TAGNYILVANTARTO_LOGIN_URL",BASE_URL."tagnyilvantarto/login/");
    if (LOGGED) {
        if (isset($_REQUEST["modul"])) {
            $m = serial($_REQUEST["modul"]);
            if (file_exists("./modul/" . $m . ".modul.php")) {
                $modul = $m;
            } else {
                $modul = "index";
            }
        }

        if (isset($request[MODUL_INDEX])) {
            $m = serial($request[MODUL_INDEX]);
            if (file_exists(BASE_DIR . "tagnyilvantarto/modul/" . $m . ".modul.php")) {
                $modul = $m;
            } else {
                $modul = "index";
            }
        }
    } else {
        $modul = "login";
    }
    $inc = "modul/" . $modul . ".modul.php";
    ob_start();

    if (!include($inc)) {
        if (DEBUG) {
            die("MODUL INC ERROR!   " . $inc);
        }
        die("MODUL INC ERROR!");
    }
    $content = ob_get_clean();
}
?>
<!DOCTYPE html>
<!--
Keret klub weboldal v1.0;
Készítette: Zsebe András "Doki" Tamás
Mail:zsebeandrastamas@gmail.com
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Keret klub tagnyilvántartó<?PHP echo $title; ?></title>
        <style>
            input {margin: 5px}
            form {text-align: left; border-style: solid;
                  border-width: 2px; border-color: #0000ff;
                  padding: 3px; margin: 3px}
            fieldset {margin-top: 10px; margin-bottom: 10px;
                      background-color: #ddffff; border-style: solid;
                      border-width: 1px; border-color: #0000ff}
            legend {color: #0000ff; font-size: 12pt;
                    font-weight: bold; font-family: Helvetica}
        </style>
    </head>
    <body>
        <header>
            <?PHP
            if(LOGGED)
            {
                ?>
            <a href="<?PHP echo BASE_URL . "tagnyilvantarto/felhasznalo_felvitel/"; ?>"><?PHP elang("Felhasználó felvitel"); ?></a> | 
            <a href="<?PHP echo BASE_URL . "tagnyilvantarto/felhasznalo_kereses/"; ?>"><?PHP elang("Felhasználó keresés"); ?></a>
            
        <?PHP
            }
        ?>
        </header>
        <?php
        echo $content;
        ?>
    </body>
</html>
