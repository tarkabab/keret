<?PHP
if (!defined("LOAD")) {
    die("CORE ERROR!!!!!");
}
$title = " - " . lang("Belépés");
if (isset($_REQUEST["login"]) && $_REQUEST["login"] == "1") {
    $ok = 1;
    if (!isset($_REQUEST["email"]) || $_REQUEST["email"] != ADMIN_MAIL) {
        $ok = 0;
    } else {
        if (!isset($_REQUEST["pwd"]) && myCrypt($_REQUEST["pwd"]) != ADMIN_PWD) {
            $ok = 0;
        }
    }
    if ($ok) {
        $_SESSION["user"]["id"]=1;
    } else {
        alert(lang("Nem megfelelő E-mail vagy jelszó!"));
    }
}

if(isset($_SESSION["user"]["id"]) && $_SESSION["user"]["id"]==1)
{
    force_redirect();
}
    
?>

<fieldset>
    <legend><?PHP echo lang("Belépés"); ?></legend>
    <form method="POST" action="<?PHP echo this_url(); ?>">
        <input type="hidden" name="login" value="1">
        <table>
            <tr><td><?PHP elang("E-mail cím"); ?>:</td><td><input type="email" value="<?PHP if ($_REQUEST["email"]) {
    echo $_REQUEST["email"];
} ?>" name="email"></td></tr>
            <tr><td><?PHP elang("Jelszó"); ?>:</td><td><input type="password" name="pwd"></td></tr>
            <tr><td colspan="2"><input type="submit" value="<?PHP elang("Belépés"); ?>"></td></tr>
        </table>
    </form>
</fieldset>