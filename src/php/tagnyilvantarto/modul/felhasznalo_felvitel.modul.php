<?php
if (!defined("LOAD")) {
    die("CORE ERROR!!!!");
}
if (!LOGGED) {
    force_redirect(TAGNYILVANTARTO_LOGIN_URL);
}
$title = " - " . lang("Felhasználó felvitel");
if (isset($_REQUEST["new"]) && $_REQUEST["new"] == 1) {
    $mehet = 1;

    if (!isset($_REQUEST["usernev"])) {
        $usernev = serial($_REQUEST["usernev"]);
    } else {
        $usernev = serial($_REQUEST["usernev"]);
    }

    if (!isset($_REQUEST["nev"])) {
        $mehet = 0;
        alert(lang("Nem megfelelő név lett megadva!"));
    } else {
        $nev = clear_query($_REQUEST["nev"]);
    }

    if (!isset($_REQUEST["szuletesi_hely"]) || strlen($_REQUEST["szuletesi_hely"]) < 2) {
        $mehet = 0;
        alert(lang("Nem megfelelő születési hely!"));
    } else {
        $szuletesi_hely = clear_query($_REQUEST["szuletesi_hely"]);
    }

    if (!isset($_REQUEST["szuletesi_ido"]) || !preg_match("/[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]/", $_REQUEST["szuletesi_ido"])) {
        $mehet = 0;
        alert(lang("Nem megfeleő születési idő!"));
    } else {
        $szuletesi_ido = clear_query($_REQUEST["szuletesi_ido"]);
    }

    if (!isset($_REQUEST["email"]) || !filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL)) {
        $mehet = 0;
        alert(lang("Nem megfeleő E-mail cím!"));
    } else {
        $email = clear_query($_REQUEST["email"]);
    }

    if (!isset($_REQUEST["jelszo"])) {
        $jelszo = myCrypt($usernev);
    } else {
        $jelszo = myCrypt($_REQUEST["jelszo"]);
    }

    if (isset($_REQUEST["megerositve"]) && ($_REQUEST["megerositve"] == 1 || $_REQUEST["megerositve"] == 0)) {
        $megerositve = $_REQUEST["megerositve"];
    } else {
        $megerositve = 0;
    }

    if (isset($_REQUEST["mail_megerositve"]) && ($_REQUEST["mail_megerositve"] == 1 || $_REQUEST["mail_megerositve"] == 0)) {
        $mail_megerositve = $_REQUEST["megerositve"];
    } else {
        $mail_megerositve = 0;
    }
    if ($mehet) {
        $q = "INSERT INTO tagok (`id`, `usernev`, `nev`, `szuletesi_hely`,"
                . " `szuletesi_ido`, `email`, `jelszo`, `megerositve`,"
                . " `mail_megerositve`, `reg_datum`, `utolso_modositas`)"
                . " VALUES (NULL, '" . $usernev . "', '" . $nev. "', '" .  $szuletesi_hely. "', '" .$szuletesi_ido. "',"
                . " '" . $email . "', '" . $jelszo . "', '" . $megerositve . "',"
                . " '" . $mail_megerositve . "', '" . date("Y-m-d") . " 12:00:00.000000', NULL);";
        database_connect();
        if (query($q)) {
            alert(lang("Sikeres felvitel :)"));
        } else {
            alert(lang("Hiba!"));
            if (DEBUG) {
                echo "<br>" . $q . "<br>";
                //alert(gettype($link));
            }
        }
    }
}
?>

<fieldset>
    <legend>
<?PHP
elang("Felhasználó felvitel");
?>
    </legend>
    <form method="POST" action="<?PHP echo this_url(); ?>">
        <input type="hidden" name="new" value=1>
        <table>
            <tr><td><?PHP elang("Felhasználónév"); ?></td><td><input type="text" name="usernev" value="<?PHP echo $_REQUEST["usernev"]; ?>"></td></tr>
            <tr><td><?PHP elang("Név"); ?>*</td><td><input type="text" name="nev" value="<?PHP echo $_REQUEST["nev"]; ?>" required="required"></td></tr>
            <tr><td><?PHP elang("Születési hely"); ?>*</td><td><input type="text" name="szuletesi_hely" value="<?PHP echo $_REQUEST["szuletesi_hely"]; ?>" required="required"></td></tr>
            <tr><td><?PHP
        elang("Születési idő");
        echo "<br><small>";
        elang("Formátum: ÉÉÉÉ-HH-NN");
        echo "</small>";
?>*</td><td><input type="date" name="szuletesi_ido" value="<?PHP echo $_REQUEST["szuletesi_ido"]; ?>" required="required"></td></tr>
            <tr><td><?PHP elang("E-mail"); ?>*</td><td><input type="email" name="email" value="<?PHP echo $_REQUEST["email"]; ?>" required="required"></td></tr>
            <tr><td><?PHP elang("Jelszó"); ?></td><td><input type="password" name="jelszo"></td></tr>
            <tr><td><?PHP elang("Megerősítve"); ?></td><td><select name="megerositve"><option value=0 selected="selected"><?PHP elang("Nem"); ?></option><option value=1><?PHP elang("Igen"); ?></option></select></td></tr>
            <tr><td><?PHP elang("E-mail megerősítve"); ?></td><td><select name="mail_megerositve"><option value=0 selected="selected"><?PHP elang("Nem"); ?></option><option value=1><?PHP elang("Igen"); ?></option></select></td></tr>
            <tr><td colspan="2"><input type="submit" value="<?PHP elang("Az adatokat leellenőriztem, és mehet! :)"); ?>"></td><td></td></tr>
        </table>
    </form>
</fieldset>

<br>