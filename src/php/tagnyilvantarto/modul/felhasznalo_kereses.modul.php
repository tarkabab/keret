<?php
if (!defined("LOAD")) {
    die("CORE ERROR!!!!");
}
if (!LOGGED) {
    force_redirect(TAGNYILVANTARTO_LOGIN_URL);
}
$title = " - " . lang("Felhasználó keresés");
if (isset($_REQUEST["search"]) && $_REQUEST["search"] == 1) {
    database_connect();

    $q = "SELECT * FROM tagok WHERE";
    $var_list = array("usernev", "nev", "szuletesi_hely", "szuletesi_ido", "email", "megerositve", "mail_megerositve");
    foreach ($var_list as $key => $value) {
        if (isset($_REQUEST[$value]) && $_REQUEST[$value] != "") {
            $q.=" " . $value . " LIKE '%" . clear_query($_REQUEST[$value]) . "%'";

            if ($value != "mail_megerositve") {
                if (isset($_REQUEST[$value . "_select"]) && $_REQUEST[$value . "_select"] == 1) {
                    $q.=" AND";
                } else {
                    $q.=" OR";
                }
            }
        }
    }
    $q.=";";
    $res = query($q);
    if (!$res) {
        alert(lang("Hibás lekérés!"));
        if (DEBUG) {
            echo "<br>" . $q . "<br>";
        }
    } else {
        $nr = num_rows($res);
        if ($nr == 0) {
            alert(lang("Nincs találat!"));
        } else {
            echo "<h2>" . lang("Találatok") . "</h2><br>";
            echo "<table>";
            $c = 0;
            echo "<tr><td>" . lang("Felhasználónév") . "</td><td>" . lang("Név") . "</td><td>" . lang("Szuletési hely") . "</td><td>" . lang("Születési idő") . "</td><td>" . lang("E-mail") . "</td><td>" . lang("Regisztráció dátuma") . "</td><td>".lang("Módosítás")."</td></tr>";
            while ($sor = fetch_array($res)) {
                if ($c % 2 == 1) {
                    $color = "lightblue";
                } else {
                    $color = "silver";
                }
                echo "<tr style='background-color:".$color.";'><td>" .$sor["usernev"]. "</td><td>" . $sor["nev"] . "</td><td>" . $sor["szuletesi_hely"] . "</td><td>" . $sor["szuletesi_ido"] . "</td><td>" . $sor["email"] . "</td><td>" . $sor["reg_datum"] . "</td><a href='".  this_url()."'><td></td></tr>";
                $c++;
            }
            echo "</table>";
        }
    }
}
?>

<fieldset>
    <legend>
<?PHP
elang("Felhasználó keresése");
?>
    </legend>
    <form method="POST" action="<?PHP echo this_url(); ?>">
        <input type="hidden" name="search" value=1>
        <table>
            <tr><td><?PHP elang("Felhasználónév"); ?></td><td><input type="text" name="usernev" value="<?PHP echo $_REQUEST["usernev"]; ?>"></td><td><select name="usernev_select"><option value=1><?PHP elang("ÉS"); ?></option><option value=0><?PHP elang("VAGY"); ?></option></select></td></tr>
            <tr><td><?PHP elang("Név"); ?></td><td><input type="text" name="nev" value="<?PHP echo $_REQUEST["nev"]; ?>"></td><td><select name="nev_select"><option value=1><?PHP elang("ÉS"); ?></option><option value=0><?PHP elang("VAGY"); ?></option></select></td></tr>
            <tr><td><?PHP elang("Születési hely"); ?></td><td><input type="text" name="szuletesi_hely" value="<?PHP echo $_REQUEST["szuletesi_hely"]; ?>"></td><td><select name="szuletesi_hely_select"><option value=1><?PHP elang("ÉS"); ?></option><option value=0><?PHP elang("VAGY"); ?></option></select></td></tr>
            <tr><td><?PHP
        elang("Születési idő");
        echo "<br><small>";
        elang("Formátum: ÉÉÉÉ-HH-NN");
        echo "</small>";
?></td><td><input type="date" name="szuletesi_ido" value="<?PHP echo $_REQUEST["szuletesi_ido"]; ?>"></td><td><select name="szuletesi_ido_select"><option value=1><?PHP elang("ÉS"); ?></option><option value=0><?PHP elang("VAGY"); ?></option></select></td></tr>
            <tr><td><?PHP elang("E-mail"); ?></td><td><input type="text" name="email" value="<?PHP echo $_REQUEST["email"]; ?>"></td><td><select name="email_select"><option value=1><?PHP elang("ÉS"); ?></option><option value=0><?PHP elang("VAGY"); ?></option></select></td></tr>
            <tr><td><?PHP elang("Megerősítve"); ?></td><td><select name="megerositve"><option value=0 selected="selected"><?PHP elang("Nem"); ?></option><option value=1><?PHP elang("Igen"); ?></option></select></td><td><select name="megerositve_select"><option value=1><?PHP elang("ÉS"); ?></option><option value=0><?PHP elang("VAGY"); ?></option></select></td></tr>
            <tr><td><?PHP elang("E-mail megerősítve"); ?></td><td><select name="mail_megerositve"><option value=0 selected="selected"><?PHP elang("Nem"); ?></option><option value=1><?PHP elang("Igen"); ?></option></select></td><td></td></tr>
            <tr><td colspan="2"><input type="submit" value="<?PHP elang("Igen, ezek érdekelnek!"); ?>"></td><td></td></tr>
        </table>
    </form>
</fieldset>

<br>