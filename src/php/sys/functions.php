<?php

function serial($string) {
    $string = str_replace(" ", "", $string);
    $string = str_replace("(", "", $string);
    $string = str_replace(")", "", $string);
    $string = str_replace(
            array('à', 'á', 'â', 'ã', 'ä', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'ő', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ű', 'ý', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', '@', ' '), array('a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', '_kukac_', "_"), $string);
    return strtolower(preg_replace('/[^(a-z|A-Z|0-9|_)]*/', "", $string));
}

function myCrypt($string) {
    return sha1(md5(sha1($string) . SALT2) . SALT1);
}

function lang($string) {
    return $string;
}

function elang($string) {
    echo lang($string);
}

function this_url() {
    return "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] . "/";
}

function nice_url($modul)
{
    //itt tartok
//    return "http://" . $_SERVER["HTTP_HOST"].
}

function alert($string) {
    echo "<script>alert('" . str_replace("'", '"', $string) . "');</script>";
}

function force_redirect($url) {
    header("Location:" . $url);
    die("<script>document.location.replace('" . $url . "');</script>");
}

function query($q) {
    global $link;
    return mysqli_query($link,$q);
}

function fetch_array($res) {
    return mysqli_fetch_array($res);
}

function num_rows($res) {
    return mysqli_num_rows($res);
}

function clear_query($s) {
    $s=  str_replace("'","_", $s);
    $s=  str_replace('"',"_", $s);
    $s=  str_replace("/","_", $s);
    $s=  str_replace("\\","_", $s);
    $s=  str_replace(";","_", $s);
    $s=  str_replace("<","_", $s);
    $s=  str_replace(">","_", $s);
    $s=  str_replace("*","_", $s);
    $s=  str_replace("(","_", $s);
    $s=  str_replace(")","_", $s);
    return $s;
}

function database_connect()
{
    global $link;
    $link = mysqli_connect(DB_HOST, DB_USER, DB_PWD);
    if (!$link) {
        die("DB CONNECT ERROR!");
    } else {
        if (!mysqli_select_db($link,DB_NAME)) {
            die("DB ERROR!");
        }
    }
}
