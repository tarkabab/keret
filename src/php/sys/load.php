<?php

session_start();

    define("LOAD", 1);


    if (isset($_SESSION["user"]) && $_SESSION["user"]["id"]) {
        define("LOGGED", 1);
    } else {
        define("LOGGED", 0);
    }
    if (!include("config.php")) {
        die("CONFIG ERROR!");
    }
    if (!include("functions.php")) {
        die("FUNCTIONS ERROR!");
    }
    $request = explode("/", $_SERVER["REQUEST_URI"]);
    if (SHIFT_NUM == 1) {
        array_shift($request);
    } elseif (SHIFT_NUM == 2) {
        array_shift($request);
        array_shift($request);
    }
    $redir_array = array("tagnyilvantarto");
    if (in_array($request[0], $redir_array)) {

        if (include(BASE_DIR . $request[0] . "/index.php")) {
            die();
        }
    } else {
        //késöbbiekben frontendfunkciók
        //TODO: megírni, amikor már tudjuk, hogy hogy is mint is kell legyen.
    }
    $link=FALSE;

